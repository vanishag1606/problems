#Print a triangular pyramid in the middle of the screen when the inout is number of rows
#We will give input in terminal only like python3 pattern.py 10
import sys
STAR , SPACE , LF = "*" , " " , "\n"
WIDTH = 60

def make_pyramid (size : int) -> list[str] :
    return [line(line_num) for line_num in range(size)]

def line (n : int) -> str :
    return (2 * n +1) * STAR

def format_pyramid(pattern : list[str]) -> list[str] :
    return LF.join(line.center(WIDTH) for line in pattern)

argc=len(sys.argv)
if argc not in (2,3) :
    print("usage:python3 pyramid.py <rows>")
elif argc == 3 :
    WIDTH = int(sys.argv[2])
    size = int (sys.argv[1])
else :
    size = int(sys.argv[1])
    if argc == 3 :
        WIDTH = int(sys.argv[2])
    print(format_pyramid(make_pyramid(size)))




