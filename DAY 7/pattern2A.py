#Print a triangular pyramid in the middle of the screen when the input is number of rows but condition being now they will have spaces between each star
#We will give input in terminal only like python3 pattern.py 10
import sys
STAR , SPACE , LF , NONE = "*" , " " , "\n",""
WIDTH = 60
START , REPEAT , FINISH = STAR , SPACE + STAR , NONE

def make_pattern (size : int) -> list[str] :
    return [line(line_num) for line_num in range(size)]

def line (n : int) -> str :
    return start(n) + middle(n) + end(n)

def start(n : int) -> str :
    return START

def middle (n : int) -> str :
    return n * REPEAT

def end(n : int) -> str :
    return NONE


def format_pyramid(pattern : list[str]) -> list[str] :
    return LF.join(line.center(WIDTH) for line in pattern)

argc=len(sys.argv)
if argc not in {2,3} :
    print("usage:python3 pyramid.py [screen width]") 
else :
    size = int(sys.argv[1])
    if argc == 3 :
        WIDTH = int(sys.argv[2])
    print(format_pyramid(make_pattern(size)))