#This is my way of representing the input. You may choose your own. str is fine, list[str] is fine, anything goes.
WHITE = ' '
BLACK = '#'
SIZE = 15
PADDED_SIZE = 17
BWW = BLACK + WHITE + WHITE

inp = '''
        #      
 # # # ### # # 
        #      
 # # # # # # # 
           ####
 # ### # # # # 
     #         
 # # # # # # # 
         #     
 # # # # ### # 
####           
 # # # # # # # 
      #        
 # # ### # # # 
      #        
'''

expected_output = [
    (0, 0, 1),
    (0, 2, 2),
    (0, 4, 3),
    (0, 6, 4),
    (0, 9, 5),
    (0, 10, 6),
    (0, 12, 7),
    (0, 14, 8),
    (2, 0, 9),
    (2, 9, 10),
    (3, 8, 11),
    (4, 0, 12),
    (5, 12, 13),
    (5, 14, 14),
    (6, 0, 15),
    (6, 4, 16),
    (6, 6, 17),
    (8, 0, 18),
    (8, 10, 19),
    (10, 4, 20),
    (10, 10, 21),
    (11, 0, 22),
    (11, 2, 23),
    (12, 0, 24),
    (12, 7, 25),
    (14, 0, 26),
    (14, 7, 27)
]

def read_grid(inp: str) -> list[str]:
    return [_ for _ in inp.split("\n") if _]

def pad(grid: list[str]) -> list[str]:
    ALL_BLACKS = BLACK * SIZE
    grid = [ALL_BLACKS] + grid + [ALL_BLACKS]
    return [BLACK + row + BLACK for row in grid]

def transpose(grid: list[str]) -> list[str]:
    return [''.join(_) for _ in zip(*grid)]    

def clue_positions(cells: str) -> list[int]:
    clue_positions = [cells.find(BWW)]
    while cells.find(BWW, clue_positions[-1]+1) != -1:
        clue_positions.append(cells.find(BWW, clue_positions[-1]+1))
    return clue_positions

def unflatten_single(clue: int) -> tuple[int, int]:
    row, col = divmod(clue, PADDED_SIZE)
    return row - 1, col

def unflatten(clues: list[int]) -> list[tuple[int, int]]:
    return [unflatten_single(item) for item in clues]


def number_crossword(inp: str) -> list[tuple[int, int, int]]:
    grid = read_grid(inp)
    padded_grid = pad(grid)
    flattened_rowwise = ''.join(padded_grid)
    flattened_columnwise = ''.join(transpose(padded_grid))
    rowwise_clues = unflatten(clue_positions(flattened_rowwise))
    columnwise_clues = [clue[::-1] for clue in unflatten(clue_positions(flattened_columnwise))]
    all_clues = set(rowwise_clues) | set(columnwise_clues)
    sorted_clues = sorted(list(all_clues), key = lambda clue: 100 * clue[0] + clue[1])
    return list(enumerate(sorted_clues, start=1))

print(number_crossword(inp))



        