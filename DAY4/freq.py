# To calculate frequency of an element
# NOT WAY APPEALINGG!
freq={}
s="ABABABA"
for ch in s :
    if ch not in freq :
        freq[ch] = 1
    else :
        freq[ch] += 1

print(freq)

# More better approach as when word will not be present it will assign a default value
from collections import defaultdict as ddict

freq2=ddict(int)
for ch in s :
    freq2[ch] += 1

print(freq2)

# Best way is to use counter function
from collections import Counter
freq3=Counter(s)
print(freq3.most_common(6))   # we can do alot using this function! abhi we are counting the most common 6 elements using this function
print(freq3)


import math
print(math.gcd(12,25))

print(math.lcm(2,3))


