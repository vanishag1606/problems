# We want to rank people in the list
# if 2 poeple got highest..then 3 people got same second highest and then 4 people got third highest
# 1 1 3 3 3 6 6 6 6 
student_data=[]
for line in open('marks.txt') :
    data = line.strip().split()
    name = data[0]
    marks = list(map(int,data[1:]))
    student_data.append((name,marks))

def final_data(student_data : list) -> list :
    total_score = []
    for name,marks in student_data: 
        marks = sum(marks)
        total_score.append((name,marks))
    return total_score
    
def sorting_marks(name_marks : list) -> list :
    scores = final_data(student_data)
    sorted_scores = sorted(scores,key = lambda score : score[1] , reverse = True)
    return sorted_scores

def assign_ranks(sorted_marks : list ) -> int :
    sorted_data = sorting_marks(student_data)
    ranked_data = []
    current_rank = 1
    previous_rank=0
    for index , (name,score) in enumerate(sorted_data) :
        if index > 0 and score == sorted_data[index-1][1] :
            current_rank += 1
            ranked_data.append((name,previous_rank,score))
            current_rank += 1
        elif index > 0 and score < sorted_data[index-1][1] :
            ranked_data.append((name,current_rank,score))
            current_rank += 1
            previous_rank += 1
    return ranked_data

print(assign_ranks(student_data))
        