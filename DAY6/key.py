#Basically same code but now e and d are reversed!
#WHY??
# For decrepting here we are using e (public key) instead of private key
# hm kisi ko message bhj rhe hai (encrept) "vanisha" using our private key (d) and ofc logo ke pass fir hmari public key(e) hogi and vo hmare sare mssges pdh skti hai (decrpt!

def breaking_pairs(s : str) -> tuple :
    length=len(s)
    pairs=[]
    for i in range(0,length,2) :
        if i+1 < length :
            pairs.append((s[i]+s[i+1]))
        else :
            pairs.append((s[i]," "))
    return pairs

def convertion (a : str , b :str ) -> int :
    return ord(a)*256 + ord(b)

def encryption(p : int , q : int , d : int , message : str) -> list :
    n=p*q
    pairs=breaking_pairs(message)  
    encrypted_list =[]
    for pair in pairs :
        x = convertion (pair[0],pair[1])
        encrypted_value = (x ** d) % n 
        encrypted_list.append(encrypted_value)
    return encrypted_list

print(encryption(293,401,11,"vanisha"))
def decryption (encrypted_list : list) -> str :
    decrypted_mess =""
    e=53091  
    for num in encrypted_list  :
        dy=(num ** e) % (293*401)
        y1,y2=divmod(dy,256)
        decrypted_mess += chr(y1)+chr(y2)
    return decrypted_mess

print( decryption([8345, 13238, 97593, 91872]))
