#Take 2 characters at a time .Read it as two digit number to the base 256 which will be called X .We can do thsat by ord(a)*256+ord(b). 
#Fix two numbers a(odd) and b(even or odd both works) together as encryption key such that a,b<128 and not equal to 0.
#We need to write Baby encryption which is E(x) = a*x+b .x is two digit num calculated above

def conversion_base256 (a : str , b : str) :
    return ord(a) * 256 + ord(b)

def encryption(num1 : int , num2 : int) :
    digit = conversion_base256('A','s') 
    return (num1*digit + num2) % (256**2)

print(encryption(23,45))
#In this way we calculated encryption!

#How to convert these encrypted numbers back to numbers!
#This is called decryption!
# Now first we will obtain our number X by (E(X)-b)/a . Now 1/a can be written as inverse of a .
#do basically ( (E[x]-b)*inverse_a ) % 256**2

def inverse (a : int) -> int :
    modulo = 0
    for b in range(100000) :
        if (a*b) % (256**2) == 1 :
            modulo = b
            break 
    return modulo


def decryption (encry : int , a :int , b : int) -> tuple :
    x = (encryption(a,b)-b)
    inverse_a=inverse(a)  
    ans = (inverse_a * x) % (256**2)
    return chr(ans//256) , chr(ans%256)

print(decryption(57730,23,45))



    

