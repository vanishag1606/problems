def breaking_pairs(s : str) -> tuple :
    length=len(s)
    pairs=[]
    for i in range(0,length,2) :
        if i+1 < length :
            pairs.append((s[i]+s[i+1]))
        else :
            pairs.append((s[i]," "))
    return pairs

def convertion (a : str , b :str ) -> int :
    return ord(a)*256 + ord(b)

def encryption(p : int , q : int , e : int , message : str) -> list :
    n=p*q
    pairs=breaking_pairs(message)   # will give you the list of message broken in pairs!
    encrypted_list =[]
    for pair in pairs :
        x = convertion (pair[0],pair[1])
        encrypted_value = (x ** e) % n 
        encrypted_list.append(encrypted_value)
    return encrypted_list

print(encryption(293,401,11,"Keep Rising!"))
def decryption (encrypted_list : list) -> str :
    decrypted_mess =""
    d=53091  # directly written (btw calculated using e**(p-1)(q-1) make separate function for that!) that concept is used a*b % m==1
    for num in encrypted_list  :
        dy=(num ** d) % (293*401)
        y1,y2=divmod(dy,256)
        decrypted_mess += chr(y1)+chr(y2)
    return decrypted_mess

print( decryption([57836, 65573, 86417, 95447, 80059, 50802, 86556]))





